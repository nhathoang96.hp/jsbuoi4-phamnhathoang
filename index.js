function sapXep() {
  var soMot = document.getElementById("so-mot").value * 1;
  var soHai = document.getElementById("so-hai").value * 1;
  var soBa = document.getElementById("so-ba").value * 1;
  if (soMot > soHai && soHai > soBa) {
    document.getElementById("result").innerHTML = `${soBa},${soHai},${soMot}`;
  } else if (soMot > soBa && soBa > soHai) {
    document.getElementById("result").innerHTML = `${soHai},${soBa},${soMot}`;
  } else if (soHai > soMot && soMot > soBa) {
    document.getElementById("result").innerHTML = `${soBa},${soMot},${soHai}`;
  } else if (soHai > soBa && soBa > soMot) {
    document.getElementById("result").innerHTML = `${soMot},${soBa},${soHai}`;
  } else if (soBa > soHai && soHai > soMot) {
    document.getElementById("result").innerHTML = `${soMot},${soHai},${soBa}`;
  } else {
    document.getElementById("result").innerHTML = `${soHai},${soMot},${soBa}`;
  }
}
// ===================================
function loiChao() {
  var thanhVien = document.getElementById("chon-tv");
  if (thanhVien.value == 5) {
    document.getElementById("result2").innerHTML = `xin chào Người lạ ơi`;
  } else {
    var strUser = thanhVien.options[thanhVien.selectedIndex].text;
    console.log("strUser: ", strUser);
    document.getElementById("result2").innerHTML = `xin chào ${strUser}`;
  }
}
// ===================================
function demSo() {
  var soM = document.getElementById("so-m").value * 1;
  var soH = document.getElementById("so-h").value * 1;
  var soB = document.getElementById("so-b").value * 1;
  var soChan = 0;
  if (soM % 2 == 0) {
    soChan++;
  }
  if (soH % 2 == 0) {
    soChan++;
  }
  if (soB % 2 == 0) {
    soChan++;
  }
  soLe = 3 - soChan;
  document.getElementById(
    "result3"
  ).innerHTML = `<p>số chẵn ${soChan} Số lẻ ${soLe}</p>`;
}
// ===================================
function duDoan() {
  var a = document.getElementById("canh-1").value * 1;
  var b = document.getElementById("canh-2").value * 1;
  var c = document.getElementById("canh-3").value * 1;

  if (a + b > c && a + c > b && b + c > a) {
    if (a == b && b == c) {
      document.getElementById("result4").innerHTML = `Tam Giác Đều`;
    } else if (a == b || a == c || b == c) {
      document.getElementById("result4").innerHTML = `Tam Giác Cân`;
    } else if (
      a * a == b * b + c * c ||
      b * b == a * a + c * c ||
      c * c == b * b + a * a
    ) {
      document.getElementById("result4").innerHTML = `Tam Giác Vuông`;
    } else {
      document.getElementById("result4").innerHTML = `Một loại tam giác khác`;
    }
  } else {
    alert("không có loại tam giác này");
  }
}
// ===================================
function ngayMai() {
  var ngay = document.getElementById("ngay").value * 1;
  var thang = document.getElementById("thang").value * 1;
  var nam = document.getElementById("nam").value * 1;
  var ngayMai = 0;
  var thangTruoc = 0;
  if (thang <= 12 && ngay <= 31 &&ngay>0) {
    if (
      thang == 1 ||
      thang == 3 ||
      thang == 5 ||
      thang == 7 ||
      thang == 8 ||
      thang == 10
    ) {
      if (ngay == 31) {
        ngayMai++;
        thang++;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngayMai}/${thang}/${nam}`;
      } else {
        ngay++;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }
    }
    if (thang == 12 && ngay <= 31) {
      if (ngay == 31) {
        ngayMai++;
        thangTruoc++;
        nam++;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngayMai}/${thangTruoc}/${nam}`;
      } else {
        ngay++;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }
    }
    if (thang == 4 || thang == 6 || thang == 9 || (thang == 11 && ngay <= 30)) {
      if (ngay == 30) {
        ngayMai++;
        thang++;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngayMai}/${thang}/${nam}`;
      }
      if (ngay < 30) {
        ngay++;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }
    }
    if (thang == 2 && nam % 4 == 0 && nam % 100 != 0 && ngay <= 29) {
        if (ngay == 29) {
          ngayMai++;
          thang++;
          document.getElementById(
            "result5"
          ).innerHTML = `${ngayMai}/${thang}/${nam}`;
        }
        if (ngay < 29) {
          ngay++;
          document.getElementById(
            "result5"
          ).innerHTML = `${ngay}/${thang}/${nam}`;
        }
      
    }
    if (thang == 2 && ngay <= 28) {
      if ((ngay == 28)) {
        ngayMai++;
        thang++;

        document.getElementById(
          "result5"
        ).innerHTML = `${ngayMai}/${thang}/${nam}`;
      } else {
        ngay++;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }
    }
  }
}


function homQua() {
  var ngay = document.getElementById("ngay").value * 1;
  var thang = document.getElementById("thang").value * 1;
  var nam = document.getElementById("nam").value * 1;
  var homQua = 31;
  var thangTruoc = 12;
  var ngayTruocNamNhuan=29;
  var ngayTruocNam=28;
  if (thang <= 12 && ngay <= 31 &&ngay>0) {
    if (
      thang == 12 ||
      thang == 5 ||
      thang == 7 ||
      thang == 8 ||
      thang == 10
    ) {
      
      if (ngay == 1) {
        homQua--;
        thang--;
        document.getElementById(
          "result5"
        ).innerHTML = `${homQua}/${thang}/${nam}`;
      } else {
        ngay--;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }
    }
    if (thang == 1 && ngay <= 31) {
      if (ngay == 1) {
        homQua==31;
        thangTruoc==12;
        nam--;
        document.getElementById(
          "result5"
        ).innerHTML = `${homQua}/${thangTruoc}/${nam}`;
      } else {
        ngay--;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }
    }
    if (thang == 4 || thang == 6 || thang == 9 || (thang == 11 && ngay <= 30)) {
      if (ngay == 1) {
        homQua--;
        thang--;
        document.getElementById(
          "result5"
        ).innerHTML = `${homQua}/${thang}/${nam}`;
      }
      if (ngay > 1) {
        ngay--;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }
    }
    if (thang == 3 && nam % 4 == 0 && nam % 100 != 0 && ngay <= 31) {
        if (ngay == 1) {
          ngayTruocNamNhuan;
          thang--;
          document.getElementById(
            "result5"
          ).innerHTML = `${ngayTruocNamNhuan}/${thang}/${nam}`;
        }
        else {
          ngay--;
          document.getElementById(
            "result5"
          ).innerHTML = `${ngay}/${thang}/${nam}`;
        }
      
    }
    if (thang == 3 && ngay <= 31) {
      if (ngay == 1) {
       ngayTruocNam;
        thang--;

        document.getElementById(
          "result5"
        ).innerHTML = `${ngayTruocNam}/${thang}/${nam}`;
      } else {
        ngay--;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }
    }
    if (thang == 2 && nam % 4 == 0 && nam % 100 != 0 && ngay <= 29){
      if (ngay==1){
        homQua--;
        thang--;
        document.getElementById(
          "result5"
        ).innerHTML = `${homQua}/${thang}/${nam}`;
      } else{
        ngay--;
        document.getElementById(
          "result5"
        ).innerHTML = `${ngay}/${thang}/${nam}`;
      }

    }
  }
}
